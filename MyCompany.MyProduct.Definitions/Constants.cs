﻿namespace MyCompany.MyProduct.Definitions
{
    public static class Constants
    {

        public const int Port = 7777;
        public const string IpAddress = "127.0.0.1";
        
    }
}
