﻿using System;

namespace MyCompany.MyProduct.Transport.Tester
{
    class Program
    {
 
        static void Main(string[] args)
        {
            var client = new TcpClient();
            client.Start();

            //press any key to exit
            Console.ReadKey();
            Environment.Exit(0);
        }
    }
}

