﻿using MyCompany.MyProduct.Definitions;
using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;

namespace MyCompany.MyProduct.Transport
{
    public class TcpClient
    {

        const int NUMBER_OF_THREADS = 2000;
        void Work(object obj)
        {
            IPEndPoint ep = new IPEndPoint(IPAddress.Parse(Constants.IpAddress), Constants.Port);
            var client = new System.Net.Sockets.TcpClient();
            client.Connect(ep);

            StringBuilder sb = new StringBuilder();
            using (NetworkStream stream = client.GetStream())
            {
                string request = "shuky" + '\0';  // DateTime.Now.ToString("MM/dd/yyyy hh:mm:ss.fff tt") + '\0';
                Console.WriteLine("sent: " + request);
                stream.Write(Encoding.ASCII.GetBytes(request), 0, request.Length);

                int i;
                while ((i = stream.ReadByte()) != 0)
                {
                    sb.Append((char)i);
                }
            }
            client.Close();

            Console.WriteLine(sb.ToString());
        }

        public void Start()
        {
            for (int i = 0; i < NUMBER_OF_THREADS; i++)
            {
                ThreadPool.QueueUserWorkItem(Work);
            }
        }
    }
}
