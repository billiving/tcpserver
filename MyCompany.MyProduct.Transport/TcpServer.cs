﻿using MyCompany.MyProduct.Definitions;
using MyCompany.MyProduct.Logger;
using MyCompany.MyProduct.Model;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;


namespace MyCompany.MyProduct.Transport
{
    public class TcpServer 
    {
        ManualResetEvent tcpClientConnected = new ManualResetEvent(false);

        void ProcessIncomingData(object obj)
        {
            System.Net.Sockets.TcpClient client = (System.Net.Sockets.TcpClient)obj;
            StringBuilder sb = new StringBuilder();

            using (NetworkStream stream = client.GetStream())
            {
                int i;
                while ((i = stream.ReadByte()) != -1)
                {
                    sb.Append((char)i);
                }

                string reply = "ack: " + sb.ToString() + '\0';
                stream.Write(Encoding.ASCII.GetBytes(reply), 0, reply.Length);
            }

            var message = JsonConvert.DeserializeObject<Message>(sb.ToString());

            ICollection<ValidationResult> validationResult;
            bool valid = GenericValidator.TryValidate(message, out validationResult);

            if (valid)
            {
                Console.WriteLine(sb.ToString());
                LogMe.Info(sb.ToString());
            }
            else
            {
                foreach (ValidationResult res in validationResult)
                {
                    Console.WriteLine($"{res.MemberNames}:{res.ErrorMessage}");
                    LogMe.Error($"{res.MemberNames}:{res.ErrorMessage}");
                }
            }

        }

        void ProcessIncomingConnection(IAsyncResult ar)
        {
            TcpListener listener = (TcpListener)ar.AsyncState;
            System.Net.Sockets.TcpClient client = listener.EndAcceptTcpClient(ar);

            ThreadPool.QueueUserWorkItem(ProcessIncomingData, client);
            tcpClientConnected.Set();
        }

        public void start()
        {
            IPEndPoint endpoint = new IPEndPoint(IPAddress.Parse(Constants.IpAddress), Constants.Port);
            TcpListener listener = new TcpListener(endpoint);
            listener.Start();
         

            while (true)
            {
                tcpClientConnected.Reset();
                listener.BeginAcceptTcpClient(new AsyncCallback(ProcessIncomingConnection), listener);
                tcpClientConnected.WaitOne();
                
            }

            
        }
    }
}
