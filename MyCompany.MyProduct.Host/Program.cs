﻿using MyCompany.MyProduct.Logger;
using MyCompany.MyProduct.Transport;

namespace MyCompany.MyProduct.Host
{
    class Program
    {
        static void Main(string[] args)
        {

            LogMe.Info("Server started..");

            var server = new TcpServer();
            server.start();
                      

        }
    }
}
