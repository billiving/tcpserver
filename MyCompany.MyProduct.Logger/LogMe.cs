﻿using log4net;
using log4net.Config;
using System.IO;
using System.Reflection;

namespace MyCompany.MyProduct.Logger
{
    public static class LogMe
    {

        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        
        static LogMe()
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            var f = new FileInfo("log4net.config");
            

            XmlConfigurator.Configure(logRepository, new FileInfo("log4net.config"));
        }
        
        public static void Info(string val)
        {
            log.Info(val);
        }


        public static void Error(string val)
        {
            log.Error(val);
        }

    }
}
